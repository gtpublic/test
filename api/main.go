package main

import (
	"log"
	"net/http"
	"os"

	"ooko/api/customer"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gopkg.in/mgo.v2"
)

var posts *mgo.Collection

func main() {

	// Connect to mongo
	session, err := mgo.Dial("mongo:27017")
	if err != nil {
		log.Fatalln(err)
		log.Fatalln("mongo err")
		os.Exit(1)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)

	// Setup router
	r := mux.NewRouter().StrictSlash(true)
	router := r.PathPrefix("/api/v1/").Subrouter()
	routerv2 := r.PathPrefix("/api/v2/").Subrouter()
	routerv2.HandleFunc("/customer", customer.AddCustomer).Methods("POST")
	router.HandleFunc("/customer", customer.AddCustomer).Methods("POST")

	http.ListenAndServe(":8080", cors.AllowAll().Handler(r))
	log.Println("Listening on port 8080...")
}
