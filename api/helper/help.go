package helper

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Response will be the structure that will be used of all responses
type Response struct {
	SUCCESS     bool   `json:"success"`
	MESSAGE     string `json:"message,omitempty"`
	ERRCATEGORY string `json:"errcategory,omitempty"`
	ACTION      int    `json:"action,omitempty"`
}

// ErrMsg to log complete error and display message to user
func (tmpResponse *Response) ErrMsg(ctx context.Context, err error, msg string, w http.ResponseWriter, r *http.Request) {
	tmpResponse.SUCCESS = false
	tmpResponse.MESSAGE = msg
	response, _ := json.Marshal(tmpResponse)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Fprintf(w, string(response))
	log.Println(msg, err.Error())
	return
}

// SuccessMsg to log complete action and display message to user
func (tmpResponse *Response) SuccessMsg(ctx context.Context, msg string, w http.ResponseWriter, r *http.Request) {
	tmpResponse.SUCCESS = true
	tmpResponse.MESSAGE = msg
	response, _ := json.Marshal(tmpResponse)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Fprintf(w, string(response))
	log.Println(msg, string(response))
}
