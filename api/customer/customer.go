package customer

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"ooko/api/helper"
	"os"
	"time"

	mgo "gopkg.in/mgo.v2"
)

// Customer struct for storing Customer profile data
type Customer struct {
	CustomerID string    `json:"customerId" bson:"customerId"`
	FirstName  string    `json:"firstName" bson:"firstName"`
	LastName   string    `json:"LastName" bson:"lastName"`
	Email      string    `json:"email" bson:"email"`
	Phone      string    `json:"phone" bson:"phone"`
	Company    string    `json:"company" bson:"company"`
	DOB        time.Time `json:"dob" bson:"dob"`
	Gender     string    `json:"gender" bson:"gender"`
	Address    string    `json:"address" bson:"address"`
	Image      string    `json:"image" bson:"image"`
	Tags       []string  `json:"tags" bson:"tags"`
	Status     string    `json:"status" bson:"status"`
}

var customers *mgo.Collection

// AddCustomer to add a new customer
func AddCustomer(w http.ResponseWriter, r *http.Request) {
	log.Println("Post API - Customer Add Called")
	var response helper.Response

	var ctx = context.Background()
	response.SuccessMsg(ctx, "Success from super messages", w, r)

	var form Customer
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&form)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	// Insert new post

	// Connect to mongo
	session, err := mgo.Dial("mongo:27017")
	if err != nil {
		log.Fatalln(err)
		log.Fatalln("mongo err")
		os.Exit(1)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)
	customers = session.DB("ooko").C("Customer")
	if err := customers.Insert(form); err != nil {
		responseError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// responseJSON(w, form)
	return
}

func responseError(w http.ResponseWriter, message string, code int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(map[string]string{"error": message})
}

func responseJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
