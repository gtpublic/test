
# Öko

**Öko** in German pronounced as ooko is same as the prefix eco in English (eg. eco-friendly)

*Developer: Greentrip Technologies*


# Installation

Download and install Docker: https://www.docker.com/community-edition#/download

**Windows:**

- Open VirtualBox
- Goto  Settings > Network Adapter 1 > Advanced > Port Forwarding
- Add a new Rule
- Leave HostIP and Guest IP empty
- Set name as   go      HostPort  8080  & GuestPort   8080
- Set name as   nginx   HostPort  8081  & GuestPort   8081
- Set name as   mongo   HostPort  27100 & GuestPort   27100

![VirtualBox Settings](vbox.png)


- Open Docker toolbox shell
- Navigate to the project directory
```docker-compose up```

**Mac**

- Navigate to the project directory
```docker-compose up```



# Development
- Download the project under $GOPATH/src/
- Install dep (https://golang.github.io/dep/docs/installation.html)
- Run ```dep ensure``` when you import new libraries 

